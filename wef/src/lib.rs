#[macro_use(js)]
extern crate webplatform;
extern crate libc;
use std::ffi::CString;
use std::rc::Rc;
use std::cell::RefCell;
use webplatform::Interop;

pub use webplatform::{init,spin,alert,Document,HtmlNode};

pub fn debugger() {
    js! { b"debugger;\0" };
}

pub fn create_tag(tag_name: &str) -> i32 {
    js! { (tag_name) b"\
        var el = document.createElement(UTF8ToString($0));\
        return WEBPLATFORM.rs_refs.push(el) - 1;\
    \0" }
}

pub fn create_text_node(content: &str) -> i32 {
    js! { (content) b"\
        var el = document.createTextNode(UTF8ToString($0));\
        return WEBPLATFORM.rs_refs.push(el) - 1;\
    \0" }
}

pub fn update_text_node(node: i32, content: &str) {
    js! { (node, content) b"\
        WEBPLATFORM.rs_refs[$0].textContent = UTF8ToString($1)\
    \0" };
}

pub fn add_child(parent: i32, child: i32) {
    js! { (parent, child) b"\
        WEBPLATFORM.rs_refs[$0].appendChild(WEBPLATFORM.rs_refs[$1]);\
    \0" };
}

pub fn add_to_body(node: i32) {
    js! { (node) b"\
        document.getElementsByTagName('body')[0].appendChild(WEBPLATFORM.rs_refs[$0]);\
    \0" };
}

pub trait Renderable: Default {
    fn create_nodes() -> Vec<i32>;
    fn update_all(&self);
    fn add_listeners(inner: Rc<RefCell<Self>>, doc: &Document);

    fn root_nodes(&self) -> Vec<i32>;

    fn add_to_body(&self) {
        for node in self.root_nodes() {
            add_to_body(node);
        }
    }
}

pub struct Component<R: Renderable> {
    inner: Rc<RefCell<R>>
}

impl<R: Renderable> Component<R> {
    pub fn new(doc: &Document) -> Self {
        let c = Component {
            inner: Rc::new(RefCell::new(R::default())),
        };
        R::add_listeners(c.inner.clone(), doc);
        c
    }

    pub fn add_to_body(&self) {
        self.inner.borrow().add_to_body();
    }
}
