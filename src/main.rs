extern crate wef;
use wef::*;

struct Counter {
    counter: i32,
    nodes: Vec<i32>,
}

impl Counter {
    pub fn incr(&mut self) {
        let c = self.counter + 1;
        self.set_counter(c);
    }

    pub fn decr(&mut self) {
        let c = self.counter - 1;
        self.set_counter(c);
    }

    
    pub fn set_counter(&mut self, counter: i32) {
        self.counter = counter;
        self.update_node6();
    }
    
    fn update_node6(&self) {
        update_text_node(self.nodes[6], { &format!("{}", self.counter)  });
    }

}

impl Default for Counter {
    fn default() -> Self {
        let c = Counter {
            
counter: i32::default(),
            nodes: Self::create_nodes(),
        };
        c.update_all();
        c
    }
}

impl Renderable for Counter {
    fn create_nodes() -> Vec<i32> {
        let nodes = vec![
            wef::create_tag("div"),
            wef::create_tag("button"),
            wef::create_text_node("+1"),
            wef::create_tag("button"),
            wef::create_text_node("-1"),
            wef::create_tag("p"),
            wef::create_text_node(""),
        ];
        
        wef::add_child(nodes[0], nodes[1]);
        wef::add_child(nodes[1], nodes[2]);
        wef::add_child(nodes[0], nodes[3]);
        wef::add_child(nodes[3], nodes[4]);
        wef::add_child(nodes[0], nodes[5]);
        wef::add_child(nodes[5], nodes[6]);
        nodes
    }

    fn update_all(&self) {
        self.update_node6();
        

    }

    fn root_nodes(&self) -> Vec<i32> {
        vec![self.nodes[0],]
    }

    fn add_listeners(inner: std::rc::Rc<std::cell::RefCell<Self>>, doc: &wef::Document) {
        

            let myinner = inner.clone();
            wef::HtmlNode { id: { let i = inner.borrow().nodes[1]; i }, doc: doc }.on("click", move |_e| {
                let mut inner = myinner.borrow_mut();
                 { inner.incr(); }
            });
        

            let myinner = inner.clone();
            wef::HtmlNode { id: { let i = inner.borrow().nodes[3]; i }, doc: doc }.on("click", move |_e| {
                let mut inner = myinner.borrow_mut();
                 { inner.decr(); }
            });
        


    }
    
}

fn main() {
    let doc = wef::init();
    let c = Component::<Counter>::new(&doc);
    c.add_to_body();
    wef::spin();
}

