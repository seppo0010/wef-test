extern crate wef;
use wef::*;

struct Counter {
    counter: i32,
}

impl Counter {
    pub fn incr(&mut self) {
        let c = self.counter + 1;
        self.set_counter(c);
    }

    pub fn decr(&mut self) {
        let c = self.counter - 1;
        self.set_counter(c);
    }

    fn render(&self) {
        <div>
            <button onclick={! |mut self, _e| { self.incr(); }!}>+1</button>
            <button onclick={! |mut self, _e| { self.decr(); }!}>-1</button>
            <p>{! &format!("{}", self.counter) !}</p>
        </div>
    }
}

fn main() {
    let doc = wef::init();
    let c = Component::<Counter>::new(&doc);
    c.add_to_body();
    wef::spin();
}
