extern crate regex;
#[macro_use]
extern crate nom;

use std::collections::HashMap;
use std::fs::File;
use std::io::Read;

use regex::Regex;
use nom::alphanumeric;

named!(tag_and_properties< (&[u8], Vec<(String, Value)> ) >, do_parse!(
    tag: take_until_either!(" >\n/=") >>
    atts: ws!(many0!(separated_pair!(take_until_either!(" >\n/="), tag!("="), alt!(
          delimited!(tag!("\""), take_until_either!("\""), tag!("\"")) => { |x: &[u8]| Value::Literal(String::from_utf8(x.to_vec()).unwrap()) }
        | delimited!(tag!("{!"), take_until!("!}"), tag!("!}")) => { |x: &[u8]| Value::Generator(String::from_utf8(x.to_vec()).unwrap()) }
    )))) >>
    ( (tag, atts.into_iter().map(|(x, y)| (String::from_utf8(x.to_owned()).unwrap(), y)).collect()) )
));

named!(open_tag< (&[u8], Vec<(String, Value)> ) >,
    ws!(delimited!(tag!("<"), tag_and_properties, tag!(">")))
);

named!(close_tag, ws!(delimited!(tag!("</"), alphanumeric, tag!(">"))));
named!(self_closing_tag< (&[u8], Vec<(String, Value)> ) >, ws!(delimited!(tag!("<"), tag_and_properties, tag!("/>"))));

named!(parse_nodes<&[u8], Vec<Node> >, many0!(node));

named!(tag<Node>, ws!(do_parse!(t: open_tag >> c: parse_nodes >> close_tag >> (Node::Tag(String::from_utf8(t.0.to_vec()).unwrap(), t.1, c)))));
named!(content<&[u8], Value >, ws!(alt!(
      delimited!(tag!("{!"), take_until!("!}"), tag!("!}")) => { |x: &[u8]| Value::Generator(String::from_utf8(x.to_vec()).unwrap()) }
    | take_until_either!("<") => { |x: &[u8]| Value::Literal(String::from_utf8(x.to_vec()).unwrap()) }
)));

named!(node<Node>, alt!(
      self_closing_tag => { |x: (&[u8], Vec<(String, Value)>)| Node::Tag(String::from_utf8(x.0.to_vec()).unwrap(), x.1, vec![]) }
    | tag => { |x| x }
    | content => { |x: Value| Node::Text(x) }
));

#[test]
fn test_self_closing_node() {
    assert_eq!(parse_nodes(b"<hello />").unwrap(), (&b""[..], vec![Node::Tag("hello".to_owned(), vec![], vec![])]));
}

#[test]
fn test_text() {
    assert_eq!(parse_nodes(b"<a>hello</a>").unwrap(), (&b""[..], vec![Node::Tag("a".to_owned(), vec![], vec![Node::Text(Value::Literal("hello".to_owned()))])]));
}

#[test]
fn test_node() {
    assert_eq!(parse_nodes(b"<hello></hello>").unwrap(), (&b""[..], vec![Node::Tag("hello".to_owned(), vec![], vec![])]));
}

#[test]
fn test_nested_node() {
    assert_eq!(parse_nodes(b"<hello><world></world></hello>").unwrap(), (&b""[..], vec![Node::Tag("hello".to_owned(), vec![], vec![
        Node::Tag("world".to_owned(), vec![], vec![])
    ])]));
}

#[test]
fn test_sibling_nodes() {
    assert_eq!(parse_nodes(b"<hello></hello><world></world>").unwrap(), (&b""[..], vec![
        Node::Tag("hello".to_owned(), vec![], vec![]),
        Node::Tag("world".to_owned(), vec![], vec![])
    ]));
}

#[test]
fn test_generator() {
    assert_eq!(parse_nodes(b"<a>{!self.content!}</a>").unwrap(), (&b""[..], vec![Node::Tag("a".to_owned(), vec![], vec![Node::Text(Value::Generator("self.content".to_owned()))])]));
}

#[test]
fn test_attributes_literal() {
    assert_eq!(parse_nodes(b"<a href=\"#\" target=\"_blank\"></a>").unwrap(), (&b""[..], vec![Node::Tag("a".to_owned(), vec![("href".to_owned(), Value::Literal("#".to_owned())), ("target".to_owned(), Value::Literal("_blank".to_owned()))], vec![])]));
}

#[test]
fn test_attributes_generator() {
    assert_eq!(parse_nodes(b"<a href={!self.href!} target={!self.target!}></a>").unwrap(), (&b""[..], vec![Node::Tag("a".to_owned(), vec![("href".to_owned(), Value::Generator("self.href".to_owned())), ("target".to_owned(), Value::Generator("self.target".to_owned()))], vec![])]));
}

#[test]
fn test_selfclosing_attributes_literal() {
    assert_eq!(parse_nodes(b"<img src=\"img.png\" alt=\"my image\" />").unwrap(), (&b""[..], vec![Node::Tag("img".to_owned(), vec![("src".to_owned(), Value::Literal("img.png".to_owned())), ("alt".to_owned(), Value::Literal("my image".to_owned()))], vec![])]));
}

#[test]
fn test_selfclosing_attributes_generator() {
    assert_eq!(parse_nodes(b"<img src={!self.src!} alt={!self.alt!} />").unwrap(), (&b""[..], vec![Node::Tag("img".to_owned(), vec![("src".to_owned(), Value::Generator("self.src".to_owned())), ("alt".to_owned(), Value::Generator("self.alt".to_owned()))], vec![])]));
}

#[derive(Debug, Clone, PartialEq)]
enum Value {
    Literal(String),
    Generator(String),
}

impl Value {
    fn uses_property(&self, property: &str) -> bool {
        match *self {
            Value::Literal(_) => false,
            Value::Generator(ref g) => g.contains(&format!("self.{}", property)),
        }
    }

    fn generator_to_listener(&self, event: &str, index: usize, code: &str) -> String {
        let re = Regex::new(r"\|\s*(mut)?\s*self\s*,\s*([A-Za-z-0-9_]+)\s*\|((?:.|\s)*)").unwrap();
        let m = re.captures_iter(code).next().unwrap();
        format!("
            let myinner = inner.clone();
            wef::HtmlNode {{ id: {{ let i = inner.borrow().nodes[{index}]; i }}, doc: doc }}.on(\"{event}\", move |{event_name}| {{
                {inner};
                {code}
            }});
        ",
        index=index,
        event=&event[2..],
        code=m.get(3).unwrap().as_str().replace("self.", "inner."),
        event_name=m.get(2).unwrap().as_str(),
        inner=if m.get(1).is_some() {
            "let mut inner = myinner.borrow_mut()"
        } else {
            "let inner = myinner.borrow()"
        }
        )
    }

    fn to_listener(&self, event: &str, index: usize) -> Option<String> {
        match *self {
            Value::Literal(_) => None,
            Value::Generator(ref code) => Some(self.generator_to_listener(event, index, code)),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Node {
    Tag(String, Vec<(String, Value)>, Vec<Node>),
    Text(Value),
}

impl Node {
    fn to_flat_node(&self, parent: Option<usize>, index: usize) -> FlatNode {
        FlatNode {
            parent: parent,
            index: index,
            content: match *self {
                Node::Tag(ref name, ref atts, _) => NodeContent::Tag(name.clone(), atts.clone()),
                Node::Text(ref v) => NodeContent::Text(v.clone()),
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum NodeContent {
    Tag(String, Vec<(String, Value)>),
    Text(Value),
}

#[derive(Debug, Clone, PartialEq)]
struct FlatNode {
    parent: Option<usize>,
    index: usize,
    content: NodeContent
}

impl FlatNode {
    fn to_source_create(&self) -> String {
        let (t, param): (&str, &str) = match self.content {
            NodeContent::Tag(ref node, _) => ("tag", node),
            NodeContent::Text(ref value) => ("text_node", match *value {
                Value::Literal(ref v) => v,
                _ => "",
            })
        };
        format!("wef::create_{}(\"{}\")", t, param)
    }

    fn is_root(&self) -> bool {
        self.parent.is_none()
    }

    fn uses_property(&self, property: &str) -> bool {
        match self.content {
            NodeContent::Tag(_, ref children) => children.iter().any(|val| val.1.uses_property(property)),
            NodeContent::Text(ref val) => val.uses_property(property),
        }
    }

    fn to_source_append(&self) -> Option<String> {
        self.parent.map(|p| format!("wef::add_child(nodes[{}], nodes[{}]);", p, self.index))
    }

    fn to_source_update(&self) -> Option<String> {
        match self.content {
            NodeContent::Tag(_, _) => None,
            NodeContent::Text(ref value) => match *value {
                Value::Generator(ref content) => Some(content.clone()),
                _ => None,
            }
        }
    }

    fn to_source_listeners(&self) -> Option<String> {
        match self.content {
            NodeContent::Tag(_, ref properties) => Some(properties.iter().flat_map(|x|
                if &x.0[0..2] == "on" { x.1.to_listener(&x.0, self.index) } else { None })),
            NodeContent::Text(_) => None,
        }.map(|x| x.fold(String::new(), |r, c| r + &c))
    }
}

fn add_nodes(parent: Option<usize>, node: Node, nodes: &mut Vec<FlatNode>) {
    let index = nodes.len();
    nodes.push(node.to_flat_node(parent, index));
    if let Node::Tag(_, _, children) = node {
        for c in children.into_iter() {
            add_nodes(Some(index), c, nodes);
        }
    }
}

fn get_nodes(x: &str) -> Vec<FlatNode> {
    let mut nodes = Vec::new();

    let parsed = parse_nodes(x.as_bytes()).unwrap();
    assert_eq!(parsed.0, { let x:&[u8] = &[]; x });
    for n in parsed.1.into_iter() {
        add_nodes(None, n, &mut nodes);
    }
    nodes
}

fn create_methods(struct_name: &str, x: &str, properties: &HashMap<String, String>) -> String {
    let nodes = get_nodes(x);

    let create_methods = nodes.iter().map(|node| {
        node.to_source_create()
    }).fold(String::new(), |r, c| r + "\n            " + &c + ",");

    let update_methods = nodes.iter().filter_map(|node| {
        node.to_source_update().map(|source| {
            format!("
    fn update_node{index}(&self) {{
        update_text_node(self.nodes[{index}], {{ {source} }});
    }}", index=node.index, source=source)
        })
    }).fold(String::new(), |r, c| r + &c + "\n");

    let append_nodes = nodes.iter().filter_map(|node| {
        node.to_source_append()
    }).fold(String::new(), |r, c| r + "\n        " + &c);

    let update_all = nodes.iter().filter_map(|node| {
        node.to_source_update().map(|_| {
            format!("
        self.update_node{index}();
        ", index=node.index)
        })
    }).fold(String::new(), |r, c| r + &c + "\n");

    let add_listeners = nodes.iter().filter_map(|node| {
        node.to_source_listeners()
    }).fold(String::new(), |r, c| r + &c + "\n");

    let root_nodes = nodes.iter().filter(|node| node.is_root()).fold(String::new(), |r, c| r + &format!("self.nodes[{}]", c.index) + ",");

    let setters = properties.iter().map(|(k, v)| {
        let updates = nodes.iter().filter(|node| node.uses_property(k)).map(|node|
            format!("\n        self.update_node{}();", node.index)
        ).fold(String::new(), |r, c| r + &c);
        format!("    pub fn set_{k}(&mut self, {k}: {v}) {{\n        self.{k} = {k};{updates}\n    }}", k=k, v=v, updates=updates)
    }).fold(String::new(), |r, c| r + "\n" + &c);

    let defaults = properties.iter().map(|(k, v)| {
        format!("{k}: {v}::default(),", k=k, v=v)
    }).fold(String::new(), |r, c| r + "\n" + &c);

    format!("{setters}
    {update_methods}
}}

impl Default for {struct_name} {{
    fn default() -> Self {{
        let c = {struct_name} {{
            {defaults}
            nodes: Self::create_nodes(),
        }};
        c.update_all();
        c
    }}
}}

impl Renderable for {struct_name} {{
    fn create_nodes() -> Vec<i32> {{
        let nodes = vec![{create_methods}
        ];
        {append_nodes}
        nodes
    }}

    fn update_all(&self) {{{update_all}
    }}

    fn root_nodes(&self) -> Vec<i32> {{
        vec![{root_nodes}]
    }}

    fn add_listeners(inner: std::rc::Rc<std::cell::RefCell<Self>>, doc: &wef::Document) {{
        {add_listeners}
    }}
    ",
    create_methods=create_methods,
    update_methods=update_methods,
    append_nodes=append_nodes,
    update_all=update_all,
    struct_name=struct_name,
    root_nodes=root_nodes,
    add_listeners=add_listeners,
    setters=setters,
    defaults=defaults,
    )
}

fn expand_string(s: &str) -> String {
    let mut data = String::new();
    let mut index = 0;
    let re = Regex::new(r"impl\s+([A-Za-z0-9]+)(?:\s|.)+(fn)\s+render\(&self\)\s*\{\n(\s*)<([a-zA-Z0-9]+)>").unwrap();
    for m in re.captures_iter(&s) {
        let render_start = m.get(2).unwrap().start();
        let spacesm = m.get(3).unwrap();
        let spaces_start = spacesm.start();
        let spaces = spacesm.end() - spacesm.start();
        let tag_name = m.get(4).unwrap().as_str();

        let struct_name = m.get(1).unwrap().as_str();
        let struct_re = Regex::new(&format!("struct\\s+{}\\s*\\{{((?:\\s*[a-zA-Z0-9]+:\\s*(?:.*)\\s*)*)\\}}", struct_name)).unwrap();
        let properties_re = Regex::new(r"([a-zA-Z0-9]+)\s*:\s*(.+)").unwrap();
        let properties_str = struct_re.captures_iter(&s).next().unwrap().get(1).unwrap().as_str();
        let properties = properties_re.captures_iter(properties_str).map(|c| {
            (c.get(1).unwrap().as_str().trim().to_owned(), c.get(2).unwrap().as_str().trim().trim_matches(',').to_owned())
        }).collect::<HashMap<_, _>>();
        let struct_match = struct_re.captures_iter(&s).next().unwrap();

        assert!(struct_match.get(0).unwrap().start() > index);
        assert!(struct_match.get(0).unwrap().end() < render_start);

        let ahre = Regex::new(&format!("\n\\s{{{}}}</{}>\\s+}}", spaces, tag_name)).unwrap();
        let end = ahre.find(&s[spaces_start..]).unwrap().end() + spaces_start - 1;

        data.push_str(&s[index..struct_match.get(0).unwrap().end() - 1]);
        data.push_str("    nodes: Vec<i32>,\n");
        data.push_str(&s[struct_match.get(0).unwrap().end() - 1..render_start]);
        data.push_str(&format!("{}", create_methods(struct_name, &s[spaces_start..end], &properties)));

        index = end + 1;
    }
    data.push_str(&s[index..]);
    data
}

fn main() {
    let mut args = std::env::args();
    args.next().unwrap();
    let mut f = File::open(args.next().unwrap()).unwrap();
    let mut s = String::new();
    f.read_to_string(&mut s).unwrap();
    println!("{}", expand_string(&s));
}

#[test]
fn test_parse_nodes() {
    assert_eq!(
        get_nodes("<div><b>Hello</b></div>"),
        vec![
            FlatNode { parent: None, index: 0, content: NodeContent::Tag("div".to_owned(), vec![]) },
            FlatNode { parent: Some(0), index: 1, content: NodeContent::Tag("b".to_owned(), vec![]) },
            FlatNode { parent: Some(1), index: 2, content: NodeContent::Text(Value::Literal("Hello".to_owned())) },
        ]
    );
}

#[test]
fn test_parse_nodes2() {
    assert_eq!(
        get_nodes("<div>Hello<b></b></div>"),
        vec![
            FlatNode { parent: None, index: 0, content: NodeContent::Tag("div".to_owned(), vec![]) },
            FlatNode { parent: Some(0), index: 1, content: NodeContent::Text(Value::Literal("Hello".to_owned())) },
            FlatNode { parent: Some(0), index: 2, content: NodeContent::Tag("b".to_owned(), vec![]) },
        ]
    );
}

#[test]
fn test_parse_nodes3() {
    assert_eq!(
        get_nodes("<div data-hello=\"hello\"></div>"),
        vec![
            FlatNode { parent: None, index: 0, content: NodeContent::Tag("div".to_owned(), vec![("data-hello".to_owned(), Value::Literal("hello".to_owned()))]) },
        ]
    );
}

#[test]
fn test_parse_nodes4() {
    assert_eq!(
        get_nodes("<div data-hello={!self.hello!}></div>"),
        vec![
            FlatNode { parent: None, index: 0, content: NodeContent::Tag("div".to_owned(), vec![("data-hello".to_owned(), Value::Generator("self.hello".to_owned()))]) },
        ]
    );
}

#[test]
fn test_parse_nodes5() {
    assert_eq!(
        get_nodes("<div>{!&format!(\"{}\", self.counter)!}</div>"),
        vec![
            FlatNode { parent: None, index: 0, content: NodeContent::Tag("div".to_owned(), vec![]) },
            FlatNode { parent: Some(0), index: 1, content: NodeContent::Text(Value::Generator("&format!(\"{}\", self.counter)".to_owned())) },
        ]
    );
}
